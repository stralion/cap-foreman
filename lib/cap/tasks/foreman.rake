namespace :foreman do
  desc <<-DESC
    Export the Procfile to upstart.

    You can override any of these defaults by setting the variables shown below.

    set :foreman_cmd,         "foreman"
    set :foreman_format,      "upstart"
    set :foreman_location,    "/etc/init"
    set :foreman_port,        5000
    set :foreman_env,         false
    set :foreman_root,        -> { current_path }
    set :foreman_procfile,    -> { release_path.join('Procfile') }
    set :foreman_app,         -> { fetch(:application) }
    set :foreman_user,        -> { fetch(:user) }
    set :foreman_log,         -> { shared_path.join('log') }
    set :foreman_pids,        false
    set :foreman_concurrency, false
    set :foreman_sudo_cmd,    "sudo"
    set :foreman_sudo,        true
    set :foreman_roles,       :all
  DESC

  task :export do
    on roles(fetch(:foreman_roles)) do
      within fetch(:release_path) do
        concurrency  = fetch(:foreman_concurrency)
        foreman_sudo = fetch(:foreman_sudo)
        foreman_sudo_cmd = fetch(:foreman_sudo_cmd)
        foreman_pids = fetch(:foreman_pids)
        foreman_env = fetch(:foreman_env)

        args  = [fetch(:foreman_format), fetch(:foreman_location)]
        args << %Q(-f #{fetch(:foreman_procfile)})
        args << %Q(-p #{fetch(:foreman_port)})
        args << %Q(-d #{fetch(:foreman_root)})
        args << %Q(-a #{fetch(:foreman_app)})
        args << %Q(-u #{fetch(:foreman_user)})
        args << %Q(-l #{fetch(:foreman_log)})
        args << %Q(-r #{fetch(:foreman_pids)}) if foreman_pids
        if foreman_env
          if foreman_env.is_a? String
            args << %Q(-e #{foreman_env})
          elsif foreman_env.is_a? Array
            foreman_env.each do |env_var|
              args << %Q(-e #{env_var})
            end
          end
        end
        args << %Q(-c #{concurrency}) if concurrency

        cmd = "#{fetch(:foreman_cmd)} export #{args.join(' ')}"

        if foreman_sudo
          if foreman_sudo_cmd == "sudo"
            execute "cd #{fetch(:release_path)} && (sudo su - -c '#{cmd}')"
          else
            execute "cd #{fetch(:release_path)} && (#{foreman_sudo_cmd} #{cmd})"
          end
        else
          execute cmd
        end
      end
    end
  end

  task :check_export do
    on roles(fetch(:foreman_roles)) do
      execute "if [[ ! -d #{fetch(:foreman_location)} ]]; then #{fetch(:foreman_sudo_cmd)} mkdir -p #{fetch(:foreman_location)}; fi"
    end
  end

  desc "Start the application services"
  task :start do
    on roles(fetch(:foreman_roles)) do
      within fetch(:release_path) do
        foreman_sudo = fetch(:foreman_sudo)
        if foreman_sudo
          execute "cd #{fetch(:release_path)} && (#{fetch(:foreman_sudo_cmd)} service #{fetch(:application)} start)"
        else
          execute "service #{fetch(:application)} start"
        end
      end
    end
  end

  desc "Stop the application services"
  task :stop do
    on roles(fetch(:foreman_roles)) do
      within fetch(:release_path) do
        foreman_sudo = fetch(:foreman_sudo)
        if foreman_sudo
          execute "cd #{fetch(:release_path)} && (#{fetch(:foreman_sudo_cmd)} service #{fetch(:application)} stop)"
        else
          execute "service #{fetch(:application)} stop"
        end
      end
    end
  end

  desc "Restart the application services"
  task :restart do
    on roles(fetch(:foreman_roles)) do
      within fetch(:release_path) do
        foreman_sudo = fetch(:foreman_sudo)
        if foreman_sudo
          execute "cd #{fetch(:release_path)} && (#{fetch(:foreman_sudo_cmd)} service #{fetch(:application)} start || #{fetch(:foreman_sudo_cmd)} service #{fetch(:application)} restart)"
        else
          execute "service #{fetch(:application)} start || service #{fetch(:application)} restart"
        end
      end
    end
  end

  before :export, :check_export
end

namespace :load do
  task :defaults do
    set :foreman_cmd,         "foreman"
    set :foreman_format,      "upstart"
    set :foreman_location,    "/etc/init"
    set :foreman_port,        5000
    set :foreman_env,         false
    set :foreman_root,        -> { current_path }
    set :foreman_procfile,    -> { release_path.join('Procfile') }
    set :foreman_app,         -> { fetch(:application) }
    set :foreman_user,        -> { fetch(:user) }
    set :foreman_log,         -> { shared_path.join('log') }
    set :foreman_pids,        false
    set :foreman_concurrency, false
    set :foreman_sudo_cmd,    "sudo"
    set :foreman_sudo,        true
    set :foreman_roles,       :all
  end
end

namespace :deploy do
  after :finished, 'foreman:export'
  after :finished, 'foreman:restart'
end
