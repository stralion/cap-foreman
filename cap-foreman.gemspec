# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = "cap-foreman"
  spec.version       = '0.1.12'
  spec.authors       = ["Juergen Busam"]
  spec.email         = ["juergen@stralion.com"]
  spec.description   = %q{Capistrano 3.x recipe for foreman}
  spec.summary       = %q{Capistrano 3.x recipe for foreman}
  spec.homepage      = "https://bitbucket.com/stralion/cap-foreman"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'capistrano', '~> 3.1'
  spec.add_dependency 'capistrano-rbenv', '~> 2.0'

  spec.add_development_dependency 'bundler', '~> 1.3'
  spec.add_development_dependency 'rake'
end
