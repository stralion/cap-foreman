# Cap-Foreman

Capistrano 3.x recipe for foreman integration

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'cap-foreman'
```

And then execute:

```bash
$ bundle install --path vendor
```

Or install it yourself as:

```bash
$ gem install cap-foreman
```

## Usage

Make sure you have `Capfile` in the root of your project, so you can add these lines (choose which one you're using):

```ruby
require 'cap/foreman'
```

Then on `config/deploy/{env}.rb` you can customize the options:

```ruby
set :foreman_procfile, -> { release_path.join('procfiles', 'staging') }
set :foreman_pids, -> { shared_path.join('pids') }
```

See the `lib/cap/tasks/*.rake` for more options.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

## Todo

1. Check presence of other 'plugins' like rbenv for creating a proper default foreman_sudo_cmd
